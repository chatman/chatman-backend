--liquibase formatted sql

--changeset agenson:create-user_role
CREATE TABLE chatman.user_role(
    user_id     BIGINT          NOT NULL    REFERENCES chatman.user(id),
    role_id     BIGINT          NOT NULL    REFERENCES chatman.role(id),
    PRIMARY KEY (user_id, role_id)
);
