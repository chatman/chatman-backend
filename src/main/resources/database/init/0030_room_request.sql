--liquibase formatted sql

--changeset agenson:create-room_request
CREATE TABLE chatman.room_request(
    id          BIGSERIAL       NOT NULL    PRIMARY KEY,
    room_id     BIGINT          NOT NULL    REFERENCES chatman.room(id),
    user_id     BIGINT          NOT NULL    REFERENCES chatman.user(id),
    created_at  VARCHAR(32)     NOT NULL,
    UNIQUE (room_id, user_id)
);
