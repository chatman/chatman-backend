--liquibase formatted sql

--changeset agenson:create-room_user
CREATE TABLE chatman.room_user(
    id          BIGSERIAL       NOT NULL    PRIMARY KEY,
    room_id     BIGINT          NOT NULL    REFERENCES chatman.room(id),
    user_id     BIGINT          NOT NULL    REFERENCES chatman.user(id),
    admin       BOOLEAN         NOT NULL,
    UNIQUE (room_id, user_id)
);
