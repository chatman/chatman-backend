--liquibase formatted sql

--changeset agenson:create-room
CREATE TABLE chatman.room(
    id          BIGSERIAL       NOT NULL    PRIMARY KEY,
    uuid        UUID            NOT NULL    UNIQUE,
    name        VARCHAR(16)     NOT NULL    UNIQUE
);
