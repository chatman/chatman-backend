--liquibase formatted sql

--changeset agenson:create-role
CREATE TABLE chatman.role(
    id          BIGSERIAL       NOT NULL    PRIMARY KEY,
    value       VARCHAR(8)      NOT NULL    UNIQUE
);
