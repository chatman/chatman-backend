--liquibase formatted sql

--changeset agenson:create-user
CREATE TABLE chatman.user(
    id          BIGSERIAL       NOT NULL    PRIMARY KEY,
    uuid        UUID            NOT NULL    UNIQUE,
    email       VARCHAR(32)     NOT NULL    UNIQUE,
    username    VARCHAR(16)     NOT NULL    UNIQUE,
    password    VARCHAR(64)     NOT NULL,
    code        UUID                        UNIQUE
);
