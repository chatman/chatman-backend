--liquibase formatted sql

--changeset agenson:populate-user
INSERT INTO chatman.user (uuid, email, username, password)
VALUES ('2496cb7a-53ab-4419-a33e-ebe4c288dab2',
        'example@gmail.com',
        'USER_DEV',
        '$2a$10$6GbEkM3cBpcQQZbnLu0ZquWhzvqV/DZrqRmZ7SDzhvsGxOsPrxAQm'); -- pwd: Pass321!
