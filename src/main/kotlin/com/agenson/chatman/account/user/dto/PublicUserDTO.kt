package com.agenson.chatman.account.user.dto

import com.agenson.chatman.account.role.Role
import com.agenson.chatman.account.user.UserDB
import java.util.*

fun UserDB.toPublicDTO() = PublicUserDTO(
    this.uuid,
    this.email,
    this.userName,
    this.roles.map { it.value }.toSet()
)

class PublicUserDTO(
    val uuid: UUID,
    val email: String,
    val username: String,
    val roles: Set<Role>
)
