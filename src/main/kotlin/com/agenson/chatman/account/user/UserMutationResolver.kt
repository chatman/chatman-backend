package com.agenson.chatman.account.user

import com.agenson.chatman.account.jwt.JwtTokenEncoder
import com.agenson.chatman.account.user.dto.TokenDTO
import com.agenson.chatman.account.role.Role
import com.agenson.chatman.account.user.dto.toTokenDTO
import com.agenson.chatman.utils.security.Unauthenticated
import graphql.kickstart.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component
import java.util.*

@Component
class UserMutationResolver(
    private val userService: UserService
): GraphQLMutationResolver {

    @Unauthenticated
    fun register(email: String, username: String, password: String): Unit =
        this.userService.register(email, username, password)

    @Unauthenticated
    fun login(username: String, password: String): TokenDTO = this.userService.login(username, password).toTokenDTO()

    @Unauthenticated
    fun verifyUser(code: UUID): Unit = this.userService.verify(code)

    fun updateUserEmail(email: String): Unit = this.userService.updateEmail(email)

    fun updateUserUsername(username: String): Unit = this.userService.updateUsername(username)

    fun updateUserPassword(oldPassword: String, newPassword: String): Unit =
        this.userService.updatePassword(oldPassword, newPassword)

    fun updateUserRole(uuid: UUID, role: Role, value: Boolean): Unit = this.userService.updateRole(uuid, role, value)

    fun deleteUser(password: String): Unit = this.userService.delete(password)
}
