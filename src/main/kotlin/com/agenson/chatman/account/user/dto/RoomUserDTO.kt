package com.agenson.chatman.account.user.dto

import com.agenson.chatman.account.role.Role
import com.agenson.chatman.account.user.UserDB
import java.lang.IllegalStateException
import java.util.*

fun UserDB.toRoomDTO(uuid: UUID) = RoomUserDTO(
    this.uuid,
    this.userName,
    this.roles.map { it.value }.toSet(),
    this.userRooms.find { it.room.uuid == uuid }?.admin
        ?: throw IllegalStateException("Cannot find user in room")
)

class RoomUserDTO(
    val uuid: UUID,
    val username: String,
    val roles: Set<Role>,
    val admin: Boolean
)
