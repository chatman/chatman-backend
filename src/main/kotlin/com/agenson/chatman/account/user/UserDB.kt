package com.agenson.chatman.account.user

import com.agenson.chatman.account.role.RoleDB
import com.agenson.chatman.chat.roomuser.RoomUserDB
import java.util.*
import javax.persistence.*
import kotlin.collections.HashSet

@Entity
@Table(schema = "chatman", name = "user")
class UserDB (
    var email: String = "",
    @Column(name = "username")
    var userName: String = "",
    @Column(name = "password")
    var passWord: String = ""
) {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = -1

    var uuid: UUID = UUID.randomUUID()

    var code: UUID? = UUID.randomUUID()

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(schema = "chatman", name = "user_role",
        joinColumns = [JoinColumn(name = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "role_id")])
    var roles: MutableSet<RoleDB> = HashSet()

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    var userRooms: MutableSet<RoomUserDB> = HashSet()
}
