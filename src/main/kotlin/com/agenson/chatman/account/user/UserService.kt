package com.agenson.chatman.account.user

import com.agenson.chatman.account.jwt.ClaimsDTO
import com.agenson.chatman.account.jwt.JwtTokenEncoder
import com.agenson.chatman.account.user.dto.TokenDTO
import com.agenson.chatman.account.role.Role
import com.agenson.chatman.account.role.RoleDB
import com.agenson.chatman.account.userdetails.UserDetailsImpl
import com.agenson.chatman.utils.auth.AuthContext
import com.agenson.chatman.utils.graphql.exception.ExceptionType
import com.agenson.chatman.utils.graphql.exception.GraphQLException
import com.agenson.chatman.utils.service.GetterService
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.util.*

@Service
class UserService(
    private val userRepository: UserRepository,
    private val passwordEncoder: BCryptPasswordEncoder,
    private val roleService: GetterService<Role, RoleDB>,
    private val authenticationManager: AuthenticationManager,
    private val mailSender: JavaMailSender,
    private val authContext: AuthContext
): GetterService<UUID, UserDB> {

    fun validEmail(email: String): Boolean =
        email.matches(("(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x0" +
                "8\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")" +
                "@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[" +
                "0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(" +
                "?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x" +
                "7f])+)\\])").toRegex())

    fun validUsername(username: String): Boolean =
        username.matches(("^(?=.{8,20}\$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])\$").toRegex())

    fun validPassword(password: String): Boolean =
        password.matches(("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$").toRegex())

    private fun sendCode(user: UserDB) {
        SimpleMailMessage()
            .also {
                it.setTo(user.email)
                it.setSubject("Verification Code")
                it.setText("Hi " + user.userName + ",\n\nPlease use this verification code to activate your account:\n"
                        + user.code.toString() + "\n\nBest regards,\nChatMan\n")
            }.also { this.mailSender.send(it) }
    }

    fun register(email: String, username: String, password: String) {
        val fields: MutableList<String> = mutableListOf()

        if (!this.validEmail(email)) fields.add("email")
        if (!this.validUsername(username)) fields.add("username")
        if (!this.validPassword(password)) fields.add("password")
        if (fields.size > 0) throw GraphQLException(ExceptionType.INVALID_INPUT, "Some fields are invalid", fields)

        if (this.userRepository.existsByEmail(email)) fields.add("email")
        if (this.userRepository.existsByUserName(username)) fields.add("username")
        if (fields.size > 0) throw GraphQLException(ExceptionType.INVALID_INPUT, "Some fields should be unique", fields)

        this.passwordEncoder.encode(password)
            .let{ encodedPassword -> UserDB(email, username, encodedPassword) }
            .also { newUser -> this.userRepository.save(newUser) }
            .also { savedUser -> this.sendCode(savedUser) }
    }

    fun verify(code: UUID) {
        this.userRepository.findByCode(code)
            ?.apply { this.code = null }
            ?.also { this.userRepository.save(it) }
            ?: throw GraphQLException(ExceptionType.INVALID_INPUT, "Code invalid", listOf("code"))
    }

    fun login(id: String, password: String): UserDB =
        UsernamePasswordAuthenticationToken(id, password)
            .let { authenticationToken -> this.authenticationManager.authenticate(authenticationToken) }
            .let { auth -> (auth.principal as UserDetailsImpl) }

    override fun get(id: UUID): UserDB = this.userRepository.findByUuid(id)
        ?: throw GraphQLException(ExceptionType.INVALID_INPUT, "User not found", listOf("uuid"))

    fun getCurrent(): UserDB = authContext.getCurrentUser()?.let { this.get(it.uuid) }
        ?: throw IllegalStateException("Cannot find current user")

    fun search(username: String): List<UserDB> =
        this.userRepository.findByUserNameContainingIgnoreCase(username)

    fun emailExists(email: String): Boolean =
        if (!this.validEmail(email))
            throw GraphQLException(ExceptionType.INVALID_INPUT, "Email invalid", listOf("email"))
        else this.userRepository.existsByEmail(email)

    fun usernameExists(username: String): Boolean =
        if (!this.validUsername(username))
            throw GraphQLException(ExceptionType.INVALID_INPUT, "Username invalid", listOf("username"))
        else this.userRepository.existsByUserName(username)

    private fun updateCurrentUser(update: UserDB.() -> Unit) {
        this.authContext.getCurrentUser()
            ?.let { currentUser ->
                this.userRepository.findByUuid(currentUser.uuid)
                    ?.apply(update)
                    ?.also { this.userRepository.save(it) }
            }
    }

    fun updateEmail(email: String) =
        if (!this.validEmail(email))
            throw GraphQLException(ExceptionType.INVALID_INPUT, "Email invalid", listOf("email"))
        else if (this.emailExists(email) && email != this.authContext.getCurrentUser()?.email)
            throw GraphQLException(ExceptionType.INVALID_INPUT, "Email already used", listOf("email"))
        else this.updateCurrentUser { this.email = email }

    fun updateUsername(username: String) =
        if (!this.validUsername(username))
            throw GraphQLException(ExceptionType.INVALID_INPUT, "Username invalid", listOf("username"))
        else if (this.usernameExists(username) && username != this.authContext.getCurrentUser()?.username)
            throw GraphQLException(ExceptionType.INVALID_INPUT, "Username already used", listOf("username"))
        else this.updateCurrentUser { this.userName = username }

    fun updatePassword(oldPassword: String, newPassword: String) {
        val fields: MutableList<String> = mutableListOf()

        if (!this.validPassword(oldPassword)) fields.add("oldPassword")
        if (!this.validPassword(newPassword)) fields.add("newPassword")
        if (fields.size > 0) throw GraphQLException(ExceptionType.INVALID_INPUT, "Some fields are invalid", fields)

        this.login(this.getCurrent().userName, oldPassword)
        this.passwordEncoder.encode(newPassword).let { encodedPassword ->
            this.updateCurrentUser { this.passWord = encodedPassword }
        }
    }

    fun updateRole(uuid: UUID, role: Role, value: Boolean) {
        this.roleService.get(role)
            .let { roleDB ->
                if (!value && this.userRepository.countAllByRolesContaining(roleDB) == 1L)
                    throw GraphQLException(ExceptionType.UNAUTHORIZED, "Cannot remove last admin")
                else this.userRepository.findByUuid(uuid)
                    ?.apply{
                        if (value) this.roles.add(roleDB)
                        else this.roles.remove(roleDB)
                    }?.also { this.userRepository.save(it) }
                    ?: throw GraphQLException(ExceptionType.INVALID_INPUT, "Cannot find user", listOf("uuid"))
            }
    }

    fun delete(password: String) {
        if (!this.validPassword(password))
            throw GraphQLException(ExceptionType.INVALID_INPUT, "Password invalid", listOf("password"))

        this.roleService.get(Role.ADMIN)
            .let { roleDB ->
                if (this.userRepository.countAllByRolesContaining(roleDB) > 1L)
                    this.getCurrent()
                        .let { currentUser ->
                            this.login(currentUser.userName, password)
                            this.userRepository.deleteByUuid(currentUser.uuid)
                        }
                else throw GraphQLException(ExceptionType.UNAUTHORIZED, "Cannot remove last admin")
            }
    }
}
