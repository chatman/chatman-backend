package com.agenson.chatman.account.user.dto

import com.agenson.chatman.account.jwt.ClaimsDTO
import com.agenson.chatman.account.jwt.JwtTokenEncoder
import com.agenson.chatman.account.jwt.toClaimsDTO
import com.agenson.chatman.account.user.UserDB
import java.util.*

fun UserDB.toTokenDTO() = TokenDTO(this.uuid)

class TokenDTO(
    uuid: UUID
) {

    val claims: ClaimsDTO = uuid.toClaimsDTO()

    val token: String = JwtTokenEncoder.encode(claims)
}
