package com.agenson.chatman.account.user.dto

import com.agenson.chatman.account.role.Role
import com.agenson.chatman.account.user.UserDB
import com.agenson.chatman.chat.room.dto.RoomDTO
import com.agenson.chatman.chat.room.dto.toDTO
import java.util.*

fun UserDB.toDTO() = UserDTO(
    this.uuid,
    this.email,
    this.userName,
    this.roles.map { it.value }.toSet(),
    this.userRooms.map { it.room.toDTO() }.toSet()
)

class UserDTO (
    val uuid: UUID,
    val email: String,
    val username: String,
    val roles: Set<Role>,
    val rooms: Set<RoomDTO>
)
