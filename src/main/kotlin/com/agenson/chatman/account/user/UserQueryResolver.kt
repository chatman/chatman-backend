package com.agenson.chatman.account.user

import com.agenson.chatman.account.role.Role
import com.agenson.chatman.account.user.dto.PublicUserDTO
import com.agenson.chatman.account.user.dto.UserDTO
import com.agenson.chatman.account.user.dto.toDTO
import com.agenson.chatman.account.user.dto.toPublicDTO
import com.agenson.chatman.utils.security.AllowedRoles
import graphql.kickstart.tools.GraphQLQueryResolver
import org.springframework.stereotype.Component
import java.util.*

@Component
class UserQueryResolver(
    private val userService: UserService
): GraphQLQueryResolver {

    fun me(): UserDTO = this.userService.getCurrent().toDTO()

    @AllowedRoles(Role.ADMIN)
    fun user(uuid: UUID): UserDTO = this.userService.get(uuid).toDTO()

    fun searchUser(username: String): List<PublicUserDTO> =
            this.userService.search(username).map { it.toPublicDTO() }

    fun userEmailExists(email: String): Boolean = this.userService.emailExists(email)

    fun userUsernameExists(username: String): Boolean = this.userService.usernameExists(username)
}
