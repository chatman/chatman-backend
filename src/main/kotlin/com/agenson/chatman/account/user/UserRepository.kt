package com.agenson.chatman.account.user

import com.agenson.chatman.account.role.RoleDB
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*
import javax.transaction.Transactional

@Repository
interface UserRepository: JpaRepository<UserDB, Long> {

    fun findByEmail(username: String): UserDB?

    fun findByUserName(username: String): UserDB?

    fun findByCode(code: UUID): UserDB?

    fun findByUserNameContainingIgnoreCase(username: String): List<UserDB>

    fun countAllByRolesContaining(role: RoleDB): Long

    fun existsByUserName(username: String): Boolean

    fun existsByEmail(email: String): Boolean

    fun findByUuid(uuid: UUID): UserDB?

    @Transactional
    fun deleteByUuid(uuid: UUID)
}
