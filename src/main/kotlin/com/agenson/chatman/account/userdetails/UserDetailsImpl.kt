package com.agenson.chatman.account.userdetails

import com.agenson.chatman.account.user.UserDB
import com.agenson.chatman.account.role.RoleDB
import com.agenson.chatman.chat.roomuser.RoomUserDB
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*

fun UserDB.toDetails() = UserDetailsImpl(
    this.id,
    this.uuid,
    this.email,
    this.userName,
    this.passWord,
    this.userRooms,
    this.roles,
    this.code
)

class UserDetailsImpl(
    id: Long,
    uuid: UUID,
    email: String,
    userName: String,
    passWord: String,
    userRooms: MutableSet<RoomUserDB>,
    roles: MutableSet<RoleDB>,
    code: UUID?
): UserDB(), UserDetails {

    init {
        this.id = id
        this.uuid = uuid
        this.email = email
        this.userName = userName
        this.passWord = passWord
        this.userRooms = userRooms
        this.roles = roles
        this.code = code
    }

    override fun getAuthorities(): List<SimpleGrantedAuthority> =
        this.roles.map { role -> SimpleGrantedAuthority(role.value.toString()) }

    override fun getPassword(): String = this.passWord

    override fun getUsername(): String = this.userName

    override fun isAccountNonExpired(): Boolean = true

    override fun isAccountNonLocked(): Boolean = true

    override fun isCredentialsNonExpired(): Boolean = true

    override fun isEnabled(): Boolean = this.code == null
}
