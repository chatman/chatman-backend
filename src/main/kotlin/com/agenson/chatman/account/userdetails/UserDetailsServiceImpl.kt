package com.agenson.chatman.account.userdetails

import com.agenson.chatman.account.user.UserDB
import com.agenson.chatman.account.user.UserRepository
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import java.util.*

@Service
class UserDetailsServiceImpl(
    private val userRepository: UserRepository
): UserDetailsService {

    override fun loadUserByUsername(id: String): UserDetailsImpl? =
        if (id.contains("@")) this.loadUser { this.userRepository.findByEmail(id) }
        else this.loadUser { this.userRepository.findByUserName(id) }

    fun loadUserByUuid(uuid: UUID): UserDetailsImpl? =
        this.loadUser { this.userRepository.findByUuid(uuid) }

    fun loadUser(userResolver: () -> UserDB?): UserDetailsImpl? = userResolver()?.toDetails()
        ?: throw BadCredentialsException("User does not exist")
}
