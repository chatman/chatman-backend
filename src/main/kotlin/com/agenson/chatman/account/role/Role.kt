package com.agenson.chatman.account.role

enum class Role {
    ADMIN;

    override fun toString(): String = "ROLE_" + super.toString()
}
