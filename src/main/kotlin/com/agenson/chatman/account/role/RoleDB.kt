package com.agenson.chatman.account.role

import javax.persistence.*

@Entity
@Table(schema = "chatman", name = "role")
class RoleDB(
    @Enumerated(EnumType.STRING)
    var value: Role
) {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = -1
}
