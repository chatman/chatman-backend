package com.agenson.chatman.account.role

import com.agenson.chatman.utils.service.GetterService
import org.springframework.stereotype.Service

@Service
class RoleService(
    private val roleRepository: RoleRepository
): GetterService<Role, RoleDB> {

    override fun get(id: Role): RoleDB = this.roleRepository.findByValue(id)
        ?: throw IllegalStateException("Cannot find $id role")
}
