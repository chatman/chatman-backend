package com.agenson.chatman.account.role

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RoleRepository: JpaRepository<RoleDB, Long> {

    fun findByValue(value: Role): RoleDB?
}
