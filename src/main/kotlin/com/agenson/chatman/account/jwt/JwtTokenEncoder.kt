package com.agenson.chatman.account.jwt

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Value

object JwtTokenEncoder {

    @Value("\${jwt.secret}")
    private lateinit var secret: String

    fun decode(token: String): ClaimsDTO = Jwts.parser()
        .setSigningKey(secret)
        .parseClaimsJws(token)
        .body.toDTO()

    fun encode(claimsDTO: ClaimsDTO): String = Jwts.builder()
        .signWith(SignatureAlgorithm.HS512, this.secret)
        .setClaims(claimsDTO.toClaims())
        .compact()
}
