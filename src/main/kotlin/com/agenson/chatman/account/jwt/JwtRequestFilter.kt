package com.agenson.chatman.account.jwt

import com.agenson.chatman.account.userdetails.UserDetailsImpl
import com.agenson.chatman.account.userdetails.UserDetailsServiceImpl
import com.agenson.chatman.utils.auth.AuthContext
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtRequestFilter(
    private val userDetailsService: UserDetailsServiceImpl,
    private val authContext: AuthContext
): OncePerRequestFilter() {

    private object CONST {
        const val HEADER: String = "Authorization"
        const val PREFIX: String = "Bearer "
    }

    @Override
    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        val jwtToken: String? = request.getHeader(CONST.HEADER)?.let { header ->
            if (header.startsWith(CONST.PREFIX)) header.substring(CONST.PREFIX.length)
            else throw IllegalArgumentException("Invalid JWT token provided")
        }

        if (jwtToken != null && this.authContext.getCurrentUser() == null) {
            val user: UserDetailsImpl? =
                try { JwtTokenEncoder.decode(jwtToken).sub?.let { uuid -> userDetailsService.loadUserByUuid(uuid) }}
                catch (_: Exception) { null }

            this.authContext.setCurrentUser(user, WebAuthenticationDetailsSource().buildDetails(request))
        }

        filterChain.doFilter(request, response)
    }
}
