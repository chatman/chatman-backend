package com.agenson.chatman.account.jwt

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken

class InvalidAuthToken: UsernamePasswordAuthenticationToken(null, null, emptyList())
