package com.agenson.chatman.account.jwt

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import java.util.*

fun UUID.toClaimsDTO() = ClaimsDTO(this)

fun Claims.toDTO(): ClaimsDTO = ClaimsDTO(
    this.subject?.let { UUID.fromString(it) },
    this.issuedAt?.time,
    this.issuer
)

fun ClaimsDTO.toClaims(): Claims = Jwts.claims()
    .setSubject(this.sub.toString())
    .setIssuedAt(Date(this.iat ?: System.currentTimeMillis()))
    .setIssuer(this.iss)


class ClaimsDTO (
    val sub: UUID? = null,
    val iat: Long? = System.currentTimeMillis(),
    val iss: String? = "chatman-backend"
)
