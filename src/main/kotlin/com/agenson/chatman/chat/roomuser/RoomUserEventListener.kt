package com.agenson.chatman.chat.roomuser

import com.agenson.chatman.utils.auth.AuthContext
import com.agenson.chatman.utils.events.RoomCreatedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
class RoomUserEventListener(
    private val roomUserService: RoomUserService,
    private val authContext: AuthContext
) {

    @EventListener
    fun handleRoomCreatedEvent(event: RoomCreatedEvent) {
        this.authContext.getCurrentUser()
            ?.let { user ->
                this.roomUserService.create(event.roomUuid, user.uuid)
                this.roomUserService.updateRole(event.roomUuid, user.uuid, true)
            }
    }
}
