package com.agenson.chatman.chat.roomuser

import com.agenson.chatman.account.user.UserDB
import com.agenson.chatman.chat.room.RoomDB
import com.agenson.chatman.utils.auth.AuthContext
import com.agenson.chatman.utils.events.RoomUserAddedEvent
import com.agenson.chatman.utils.graphql.exception.ExceptionType
import com.agenson.chatman.utils.graphql.exception.GraphQLException
import com.agenson.chatman.utils.service.GetterService
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import java.util.*

@Service
class RoomUserService(
    private val roomUserRepository: RoomUserRepository,
    private val eventPublisher: ApplicationEventPublisher,
    private val roomService: GetterService<UUID, RoomDB>,
    private val userService: GetterService<UUID, UserDB>,
    private val authContext: AuthContext
) {

    fun create(roomUuid: UUID, userUuid: UUID) {
        val room: RoomDB = this.roomService.get(roomUuid)
        val user: UserDB = this.userService.get(userUuid)

        this.roomUserRepository.save(RoomUserDB(room, user))
        this.eventPublisher.publishEvent(RoomUserAddedEvent(this, roomUuid, userUuid))
    }

    fun updateRole(roomUuid: UUID, userUuid: UUID, admin: Boolean) {
        val room: RoomDB = this.roomService.get(roomUuid)

        room.roomUsers.find { it.user.uuid == userUuid }
            ?.apply {
                if (!admin && this.admin && room.roomUsers.count { it.admin } == 1)
                    throw GraphQLException(ExceptionType.UNAUTHORIZED, "Cannot remove last admin in room")
                else this.admin = admin
            } ?.let { this.roomUserRepository.save(it) }
            ?: throw GraphQLException(ExceptionType.INVALID_INPUT, "Cannot find user in room", listOf("userUuid"))
    }

    fun delete(roomUuid: UUID, userUuid: UUID) {
        val room: RoomDB = this.roomService.get(roomUuid)

        room.roomUsers.find { it.user.uuid == userUuid }
            ?.let { roomUser ->
                if (roomUser.admin && room.roomUsers.count { it.admin } == 1)
                    throw GraphQLException(ExceptionType.UNAUTHORIZED, "Cannot remove last admin in room")
                else this.roomUserRepository.delete(roomUser)
            } ?: throw GraphQLException(ExceptionType.INVALID_INPUT, "Cannot find user in room", listOf("userUuid"))
    }

    fun leave(uuid: UUID) {
        val room: RoomDB = this.roomService.get(uuid)

        this.authContext.getCurrentUser()?.let { user ->
            user.userRooms.find { it.room.uuid == uuid }
                ?.let { roomUser ->
                    if (roomUser.admin && room.roomUsers.count { it.admin } == 1)
                        throw GraphQLException(ExceptionType.UNAUTHORIZED, "Cannot remove last admin in room")
                    else this.roomUserRepository.delete(roomUser) }
                ?: throw GraphQLException(ExceptionType.INVALID_INPUT, "Cannot find user in room", listOf("uuid"))
        }
    }
}
