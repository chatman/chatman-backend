package com.agenson.chatman.chat.roomuser

import com.agenson.chatman.utils.security.RoomMember
import graphql.kickstart.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component
import java.util.*

@Component
class RoomUserMutationResolver(
    private val roomUserService: RoomUserService
): GraphQLMutationResolver {

    @RoomMember(admin = true)
    fun addRoomUser(roomUuid: UUID, userUuid: UUID): Unit = this.roomUserService.create(roomUuid, userUuid)

    @RoomMember(admin = true)
    fun updateRoomUserRole(roomUuid: UUID, userUuid: UUID, admin: Boolean): Unit =
        this.roomUserService.updateRole(roomUuid, userUuid, admin)

    @RoomMember(admin = true)
    fun removeRoomUser(roomUuid: UUID, userUuid: UUID): Unit = this.roomUserService.delete(roomUuid, userUuid)

    @RoomMember
    fun leaveRoom(uuid: UUID): Unit = this.roomUserService.leave(uuid)
}
