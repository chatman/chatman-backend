package com.agenson.chatman.chat.roomuser

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RoomUserRepository: JpaRepository<RoomUserDB, Long>
