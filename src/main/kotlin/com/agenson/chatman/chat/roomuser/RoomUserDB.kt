package com.agenson.chatman.chat.roomuser

import com.agenson.chatman.account.user.UserDB
import com.agenson.chatman.chat.room.RoomDB
import javax.persistence.*

@Entity
@Table(schema = "chatman", name = "room_user")
class RoomUserDB(
    @ManyToOne
    @JoinColumn(name = "room_id")
    var room: RoomDB,
    @ManyToOne
    @JoinColumn(name = "user_id")
    var user: UserDB,
    var admin: Boolean = false
) {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = -1
}
