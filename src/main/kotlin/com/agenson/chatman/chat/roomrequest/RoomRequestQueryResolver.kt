package com.agenson.chatman.chat.roomrequest

import com.agenson.chatman.utils.security.RoomMember
import graphql.kickstart.tools.GraphQLQueryResolver
import org.springframework.stereotype.Component
import java.util.*

@Component
class RoomRequestQueryResolver(
    private val roomRequestService: RoomRequestService
): GraphQLQueryResolver {

    @RoomMember(admin = true)
    fun roomRequests(uuid: UUID): List<RoomRequestDTO> = this.roomRequestService.search(uuid).map { it.toDTO() }
}
