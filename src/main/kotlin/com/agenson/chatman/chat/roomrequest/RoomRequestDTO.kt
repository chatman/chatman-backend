package com.agenson.chatman.chat.roomrequest

import com.agenson.chatman.account.user.dto.PublicUserDTO
import com.agenson.chatman.account.user.dto.toPublicDTO
import java.time.OffsetDateTime

fun RoomRequestDB.toDTO() = RoomRequestDTO(
    this.user.toPublicDTO(),
    this.createdAt
)

class RoomRequestDTO(
    val user: PublicUserDTO,
    val createdAt: OffsetDateTime
)
