package com.agenson.chatman.chat.roomrequest

import graphql.kickstart.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component
import java.util.*

@Component
class RoomRequestMutationResolver(
    private val roomRequestService: RoomRequestService
): GraphQLMutationResolver {

    fun createRoomRequest(uuid: UUID): Unit = this.roomRequestService.create(uuid)
}
