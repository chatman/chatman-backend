package com.agenson.chatman.chat.roomrequest

import com.agenson.chatman.account.user.UserDB
import com.agenson.chatman.chat.room.RoomDB
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface RoomRequestRepository: JpaRepository<RoomRequestDB, Long> {

    fun findByRoomUuid(uuid: UUID): List<RoomRequestDB>

    fun deleteByRoomAndUser(room: RoomDB, user: UserDB)
}
