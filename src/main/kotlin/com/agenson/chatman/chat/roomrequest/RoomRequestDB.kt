package com.agenson.chatman.chat.roomrequest

import com.agenson.chatman.account.user.UserDB
import com.agenson.chatman.chat.room.RoomDB
import java.time.OffsetDateTime
import javax.persistence.*

@Entity
@Table(schema = "chatman", name = "room_request")
class RoomRequestDB(
    @ManyToOne
    @JoinColumn(name = "room_id")
    var room: RoomDB,
    @ManyToOne
    @JoinColumn(name = "user_id")
    var user: UserDB
) {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = -1

    var createdAt: OffsetDateTime = OffsetDateTime.now()
}
