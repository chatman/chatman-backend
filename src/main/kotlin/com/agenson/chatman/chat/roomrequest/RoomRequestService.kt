package com.agenson.chatman.chat.roomrequest

import com.agenson.chatman.account.user.UserDB
import com.agenson.chatman.chat.room.RoomDB
import com.agenson.chatman.utils.auth.AuthContext
import com.agenson.chatman.utils.service.GetterService
import org.springframework.stereotype.Service
import java.util.*

@Service
class RoomRequestService(
    private val roomRequestRepository: RoomRequestRepository,
    private val roomService: GetterService<UUID, RoomDB>,
    private val userService: GetterService<UUID, UserDB>,
    private val authContext: AuthContext
) {

    fun create(roomUuid: UUID) {
        this.roomService.get(roomUuid)
            .let { room ->
                this.authContext.getCurrentUser()?.let { user ->
                    this.roomRequestRepository.save(RoomRequestDB(room, user))
                }
            }
    }

    fun search(roomUuid: UUID): List<RoomRequestDB> =
        this.roomRequestRepository.findByRoomUuid(roomUuid)

    fun delete(roomUuid: UUID, userUuid: UUID) {
        val room: RoomDB = this.roomService.get(roomUuid)
        val user: UserDB = this.userService.get(userUuid)

        this.roomRequestRepository.deleteByRoomAndUser(room, user)
    }
}
