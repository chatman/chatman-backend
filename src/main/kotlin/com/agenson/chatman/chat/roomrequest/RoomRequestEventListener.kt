package com.agenson.chatman.chat.roomrequest

import com.agenson.chatman.utils.events.RoomUserAddedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
class RoomRequestEventListener(
    private val roomRequestService: RoomRequestService
) {

    @EventListener
    fun handleRoomUserAddedEvent(event: RoomUserAddedEvent) {
        this.roomRequestService.delete(event.roomUuid, event.userUuid)
    }
}
