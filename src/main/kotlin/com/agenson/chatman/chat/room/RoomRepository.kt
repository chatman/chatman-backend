package com.agenson.chatman.chat.room

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface RoomRepository: JpaRepository<RoomDB, Long> {

    fun findByUuid(uuid: UUID): RoomDB?

    fun existsByName(name: String): Boolean

    fun findByNameContainingIgnoreCase(name: String): List<RoomDB>
}
