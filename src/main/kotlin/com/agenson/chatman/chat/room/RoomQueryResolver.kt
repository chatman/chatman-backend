package com.agenson.chatman.chat.room

import com.agenson.chatman.chat.room.dto.PublicRoomDTO
import com.agenson.chatman.chat.room.dto.RoomDTO
import com.agenson.chatman.chat.room.dto.toDTO
import com.agenson.chatman.chat.room.dto.toPublicDTO
import com.agenson.chatman.utils.security.RoomMember
import graphql.kickstart.tools.GraphQLQueryResolver
import org.springframework.stereotype.Component
import java.util.*

@Component
class RoomQueryResolver(
    private val roomService: RoomService
): GraphQLQueryResolver {

    @RoomMember
    fun room(uuid: UUID): RoomDTO = this.roomService.get(uuid).toDTO()

    fun searchRoom(name: String): List<PublicRoomDTO> = this.roomService.search(name).map { it.toPublicDTO() }

    fun roomNameExists(name: String): Boolean = this.roomService.nameExists(name)
}
