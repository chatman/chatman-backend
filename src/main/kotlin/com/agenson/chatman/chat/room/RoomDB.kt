package com.agenson.chatman.chat.room

import com.agenson.chatman.chat.roomuser.RoomUserDB
import java.util.*
import javax.persistence.*

@Entity
@Table(schema = "chatman", name = "room")
class RoomDB(
    var name: String = ""
) {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = -1

    var uuid: UUID = UUID.randomUUID()

    @OneToMany(mappedBy = "room", fetch = FetchType.EAGER)
    var roomUsers: MutableSet<RoomUserDB> = HashSet()
}
