package com.agenson.chatman.chat.room

import com.agenson.chatman.chat.room.dto.RoomDTO
import com.agenson.chatman.chat.room.dto.toDTO
import com.agenson.chatman.utils.security.RoomMember
import graphql.kickstart.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component
import java.util.*

@Component
class RoomMutationResolver(
    private val roomService: RoomService
): GraphQLMutationResolver {

    fun createRoom(name: String): RoomDTO = this.roomService.create(name).toDTO()

    @RoomMember(admin = true)
    fun updateRoomName(uuid: UUID, name: String): Unit = this.roomService.updateName(uuid, name)

    @RoomMember(admin = true)
    fun deleteRoom(uuid: UUID): Unit = this.roomService.delete(uuid)
}
