package com.agenson.chatman.chat.room

import com.agenson.chatman.utils.events.RoomCreatedEvent
import com.agenson.chatman.utils.graphql.exception.ExceptionType
import com.agenson.chatman.utils.graphql.exception.GraphQLException
import com.agenson.chatman.utils.service.GetterService
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import java.util.*

@Service
class RoomService(
    private val roomRepository: RoomRepository,
    private val eventPublisher: ApplicationEventPublisher
): GetterService<UUID, RoomDB> {

    fun validName(name: String): Boolean =
        name.matches(("^(?=.{4,16}\$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])\$").toRegex())

    fun create(name: String): RoomDB {
        if (!this.validName(name))
            throw GraphQLException(ExceptionType.INVALID_INPUT, "Name invalid", listOf("name"))
        else if (this.roomRepository.existsByName(name))
            throw GraphQLException(ExceptionType.INVALID_INPUT, "Name should be unique", listOf("name"))

        return this.roomRepository.save(RoomDB(name)).uuid
            .also { this.eventPublisher.publishEvent(RoomCreatedEvent(this, it)) }
            .let { this.get(it) }
    }

    override fun get(id: UUID): RoomDB = this.roomRepository.findByUuid(id)
        ?: throw GraphQLException(ExceptionType.INVALID_INPUT, "Room not found", listOf("uuid"))

    fun nameExists(name: String): Boolean =
        if (!this.validName(name))
            throw GraphQLException(ExceptionType.INVALID_INPUT, "Name invalid", listOf("name"))
        else this.roomRepository.existsByName(name)

    fun search(name: String): List<RoomDB> =
        this.roomRepository.findByNameContainingIgnoreCase(name)

    fun updateName(uuid: UUID, name: String) {
        val room: RoomDB = this.roomRepository.findByUuid(uuid)
            ?: throw GraphQLException(ExceptionType.INVALID_INPUT, "Room not found", listOf("uuid"))

        if (!this.validName(name))
            throw GraphQLException(ExceptionType.INVALID_INPUT, "Name invalid", listOf("name"))
        else if (this.roomRepository.existsByName(name) && name != room.name)
            throw GraphQLException(ExceptionType.INVALID_INPUT, "Name should be unique", listOf("name"))

        room.apply { this.name = name }
            .also { this.roomRepository.save(it) }
    }

    fun delete(uuid: UUID) = this.get(uuid).let { this.roomRepository.delete(it) }
}
