package com.agenson.chatman.chat.room.dto

import com.agenson.chatman.chat.room.RoomDB
import java.util.*

fun RoomDB.toPublicDTO() = PublicRoomDTO(
    this.uuid,
    this.name
)

class PublicRoomDTO(
    val uuid: UUID,
    val name: String
)
