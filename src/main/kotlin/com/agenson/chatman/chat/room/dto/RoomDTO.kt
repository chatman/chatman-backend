package com.agenson.chatman.chat.room.dto

import com.agenson.chatman.account.user.dto.RoomUserDTO
import com.agenson.chatman.account.user.dto.toRoomDTO
import com.agenson.chatman.chat.room.RoomDB
import java.util.*

fun RoomDB.toDTO() = RoomDTO(
    this.uuid,
    this.name,
    this.roomUsers.map { it.user.toRoomDTO(this.uuid) }.toSet()
)

class RoomDTO(
    val uuid: UUID,
    val name: String,
    val users: Set<RoomUserDTO>
)
