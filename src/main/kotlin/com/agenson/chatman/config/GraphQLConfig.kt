package com.agenson.chatman.config

import com.agenson.chatman.utils.graphql.converters.LongScalarTypeFactory
import com.agenson.chatman.utils.graphql.converters.OffsetDateTimeScalarTypeFactory
import com.agenson.chatman.utils.graphql.converters.UuidScalarTypeFactory
import graphql.schema.GraphQLScalarType
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class GraphQLConfig {

    @Bean
    internal fun uuidScalarType(uuidScalarTypeFactory: UuidScalarTypeFactory): GraphQLScalarType? =
        uuidScalarTypeFactory.getObject()

    @Bean
    internal fun longScalarType(longScalarTypeFactory: LongScalarTypeFactory): GraphQLScalarType? =
        longScalarTypeFactory.getObject()

    @Bean
    internal fun offsetDateTimeScalarType(
        offsetDateTimeScalarTypeFactory: OffsetDateTimeScalarTypeFactory
    ): GraphQLScalarType? = offsetDateTimeScalarTypeFactory.getObject()
}
