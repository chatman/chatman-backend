package com.agenson.chatman.utils.events

import org.springframework.context.ApplicationEvent
import java.util.*

class RoomUserAddedEvent(
    source: Any,
    val roomUuid: UUID,
    val userUuid: UUID
): ApplicationEvent(source)
