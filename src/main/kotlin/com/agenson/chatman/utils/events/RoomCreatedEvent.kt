package com.agenson.chatman.utils.events

import org.springframework.context.ApplicationEvent
import java.util.*

class RoomCreatedEvent(
    source: Any,
    val roomUuid: UUID
): ApplicationEvent(source)
