package com.agenson.chatman.utils.security

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class RoomMember(
    val admin: Boolean = false
)
