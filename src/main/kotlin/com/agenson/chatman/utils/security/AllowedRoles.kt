package com.agenson.chatman.utils.security

import com.agenson.chatman.account.role.Role

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class AllowedRoles(
    vararg val roles: Role
)
