package com.agenson.chatman.utils.security

import com.agenson.chatman.account.jwt.InvalidAuthToken
import com.agenson.chatman.account.userdetails.UserDetailsImpl
import com.agenson.chatman.utils.auth.AuthContext
import com.agenson.chatman.utils.graphql.exception.ExceptionType
import com.agenson.chatman.utils.graphql.exception.GraphQLException
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.aspectj.lang.reflect.MethodSignature
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import java.lang.IllegalStateException
import java.util.*

@Aspect
@Component
class SecurityHandler(
    private val authContext: AuthContext
) {

    @Before("this(graphql.kickstart.tools.GraphQLResolver) && !@annotation(Unauthenticated)")
    fun checkAuthentication() {
        this.assertAuthenticated()
    }

    @Before("@annotation(AllowedRoles)")
    fun checkRestrictions(joinPoint: JoinPoint) {
        val allowedAuthorities: List<SimpleGrantedAuthority> = (joinPoint.signature as MethodSignature)
            .method.getAnnotation(AllowedRoles::class.java)
            .roles.map { SimpleGrantedAuthority(it.toString()) }

        if (this.assertAuthenticated().authorities.none { allowedAuthorities.contains(it) })
            throw GraphQLException(ExceptionType.UNAUTHORIZED, "Operation requires authorization")
    }

    @Before("@annotation(RoomMember)")
    fun checkRestrictionsWithRoom(joinPoint: JoinPoint) {
        val admin: Boolean = (joinPoint.signature as MethodSignature).method.getAnnotation(RoomMember::class.java).admin
        val roomUuid: UUID = joinPoint.args.takeIf { it.isNotEmpty() }?.first() as? UUID
            ?: throw IllegalStateException("Method first parameter should be the room uuid")
        val user: UserDetailsImpl = this.assertAuthenticated().principal as UserDetailsImpl

        if (user.userRooms.none { it.room.uuid == roomUuid })
            throw GraphQLException(ExceptionType.UNAUTHORIZED, "User is not a member of this room")
        if (admin && user.userRooms.none { it.room.uuid == roomUuid && it.admin })
            throw GraphQLException(ExceptionType.UNAUTHORIZED, "User is not an admin of this room")
    }

    private fun assertAuthenticated() = this.authContext.getCurrentAuth()
        ?.takeUnless { it is AnonymousAuthenticationToken || !it.isAuthenticated }
        ?.also { if (it is InvalidAuthToken) throw GraphQLException(ExceptionType.INVALID_TOKEN, "Invalid JWT token") }
        ?: throw GraphQLException(ExceptionType.UNAUTHENTICATED, "Operation requires authentication")
}
