package com.agenson.chatman.utils.database

import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter(autoApply = true)
class OffsetDateTimeConverter: AttributeConverter<OffsetDateTime, String> {

    override fun convertToDatabaseColumn(attribute: OffsetDateTime?): String? =
            attribute?.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)

    override fun convertToEntityAttribute(dbData: String?): OffsetDateTime? =
            dbData?.let { OffsetDateTime.parse(it, DateTimeFormatter.ISO_OFFSET_DATE_TIME) }
}
