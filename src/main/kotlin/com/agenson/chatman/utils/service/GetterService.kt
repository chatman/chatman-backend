package com.agenson.chatman.utils.service

interface GetterService<S, T> {

    fun get(id: S): T
}
