package com.agenson.chatman.utils.graphql.exception

import graphql.GraphqlErrorException

open class GraphQLException(
    type: ExceptionType = ExceptionType.UNKNOWN,
    message: String = "Unknown error",
    fields: List<String>? = null
): GraphqlErrorException(
    newErrorException()
        .message(message)
        .extensions(mutableMapOf<String, Any>("type" to type, "message" to message)
            .apply { if (fields != null) this["fields"] = fields })
)
