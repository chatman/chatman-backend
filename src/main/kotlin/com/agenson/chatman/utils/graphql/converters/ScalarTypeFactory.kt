package com.agenson.chatman.utils.graphql.converters

import graphql.language.StringValue
import graphql.schema.*
import org.springframework.beans.factory.FactoryBean

abstract class ScalarTypeFactory<T>(
    private val name: String,
    private val description: String = "$name Scalar"
): FactoryBean<GraphQLScalarType> {

    protected abstract fun encodeString(value: T): String

    protected abstract fun decodeString(value: String): T

    protected abstract fun safeCast(value: Any): T?

    override fun getObject(): GraphQLScalarType? = GraphQLScalarType.newScalar()
        .name(this.name)
        .description(this.description)
        .coercing(object: Coercing<T, String> {

            @Throws(CoercingParseValueException::class)
            override fun parseValue(input: Any): T {
                if (input !is String) throw CoercingParseValueException("Incorrect type")
                try {
                    return decodeString(input)
                } catch (e: IllegalArgumentException) {
                    throw CoercingParseValueException(e)
                }
            }

            @Throws(CoercingParseLiteralException::class)
            override fun parseLiteral(input: Any): T {
                if (input !is StringValue) throw CoercingParseLiteralException("Incorrect type")
                try {
                    return decodeString(input.value)
                } catch (e: IllegalArgumentException) {
                    throw CoercingParseLiteralException(e)
                }
            }

            @Throws(CoercingSerializeException::class)
            override fun serialize(dataFetcherResult: Any): String =
                safeCast(dataFetcherResult)?.let { encodeString(it) }
                    ?:throw CoercingSerializeException("Incorrect type")
        })
        .build()

    override fun getObjectType(): Class<*> = GraphQLScalarType::class.java

    override fun isSingleton(): Boolean = true
}
