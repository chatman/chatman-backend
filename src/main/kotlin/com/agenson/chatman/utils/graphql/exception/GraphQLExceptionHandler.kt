package com.agenson.chatman.utils.graphql.exception

import graphql.ExceptionWhileDataFetching
import graphql.GraphQLError
import graphql.kickstart.execution.error.GraphQLErrorHandler
import graphql.validation.ValidationError
import org.springframework.security.core.AuthenticationException
import org.springframework.stereotype.Component
import java.util.stream.Collectors

@Component
class GraphQLExceptionHandler: GraphQLErrorHandler {

    override fun processErrors(list: List<GraphQLError>): List<GraphQLError> =
        list.stream().map { formatError(it) }.collect(Collectors.toList())

    private fun formatError(error: GraphQLError): GraphQLError = when (error) {
        is ExceptionWhileDataFetching -> when (error.exception) {
            is AuthenticationException -> GraphQLException(ExceptionType.INVALID_INPUT, "Invalid credentials")
            is GraphQLError -> error.exception as GraphQLError
            else -> GraphQLException()
        }
        is ValidationError -> error
        else -> GraphQLException()
    }
}
