package com.agenson.chatman.utils.graphql.converters

import org.springframework.stereotype.Component
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

@Component
class OffsetDateTimeScalarTypeFactory: ScalarTypeFactory<OffsetDateTime>("OffsetDateTime") {

    override fun encodeString(value: OffsetDateTime): String = value.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)

    override fun decodeString(value: String): OffsetDateTime =
            OffsetDateTime.parse(value, DateTimeFormatter.ISO_OFFSET_DATE_TIME)

    override fun safeCast(value: Any): OffsetDateTime? = value as? OffsetDateTime
}
