package com.agenson.chatman.utils.graphql.converters

import org.springframework.stereotype.Component

@Component
class LongScalarTypeFactory: ScalarTypeFactory<Long>("Long") {

    override fun encodeString(value: Long): String = value.toString()

    override fun decodeString(value: String): Long = value.toLong()

    override fun safeCast(value: Any): Long? = value as? Long
}
