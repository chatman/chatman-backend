package com.agenson.chatman.utils.graphql.exception

enum class ExceptionType {
    INVALID_TOKEN,
    UNAUTHENTICATED,
    UNAUTHORIZED,
    INVALID_INPUT,
    UNKNOWN
}
