package com.agenson.chatman.utils.graphql.converters

import org.springframework.stereotype.Component
import java.util.*

@Component
class UuidScalarTypeFactory: ScalarTypeFactory<UUID>("UUID") {

    override fun encodeString(value: UUID): String = value.toString()

    override fun decodeString(value: String): UUID = UUID.fromString(value)

    override fun safeCast(value: Any): UUID? = value as? UUID
}
