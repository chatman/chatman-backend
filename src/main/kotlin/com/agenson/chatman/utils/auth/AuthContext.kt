package com.agenson.chatman.utils.auth

import com.agenson.chatman.account.userdetails.UserDetailsImpl
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.WebAuthenticationDetails

interface AuthContext {

    fun getCurrentAuth(): Authentication?

    fun getCurrentUser(): UserDetailsImpl?

    fun setCurrentUser(user: UserDetailsImpl?, details: WebAuthenticationDetails)
}
