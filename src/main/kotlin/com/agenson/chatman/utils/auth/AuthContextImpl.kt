package com.agenson.chatman.utils.auth

import com.agenson.chatman.account.jwt.InvalidAuthToken
import com.agenson.chatman.account.userdetails.UserDetailsImpl
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetails
import org.springframework.stereotype.Component

@Component
class AuthenticationContextImpl: AuthContext {

    override fun getCurrentAuth(): Authentication? = SecurityContextHolder.getContext().authentication

    override fun getCurrentUser(): UserDetailsImpl? = this.getCurrentAuth()?.principal as? UserDetailsImpl?

    override fun setCurrentUser(user: UserDetailsImpl?, details: WebAuthenticationDetails) {
        SecurityContextHolder.getContext().authentication =
            user.let {
                if (it == null) InvalidAuthToken()
                else UsernamePasswordAuthenticationToken(it, null, it.authorities)
            }.apply { this.details = details }
    }
}
